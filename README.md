# README

## Requirements

- ruby 2.2.0 (e.g. via `rvm.io`, `rvm install ruby-2.2.0`)
- bundler: `gem install bundler`

## Installation

Run `bundle` inside project root

## Starting the server

- Development: `rails s`
- Production: `RAILS_ENV=production rails s` (shortcut via `./production.sh`)

# Generating static website (with wget)

1. Run production server with `./production.sh`
2. Run `./crawl.sh` while production server is running to generate a static website mirror under `dist`

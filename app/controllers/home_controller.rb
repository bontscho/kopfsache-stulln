class HomeController < ApplicationController
  def impressum
    render :impressum
  end

  def team
    render :team
  end

  def salon
    render :salon
  end

  def service
    render :service
  end
end
# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
KopfsacheStulln::Application.config.secret_key_base = 'b0a6c3cb7d1f7901e5c8f796531527f04ff67af17f78086b6872a53b6dac82522d14950ee58c90ad27fdf772d5a810f2ee5986d112f0031b0fc398b0e5029680'
